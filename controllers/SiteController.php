<?php

namespace app\controllers;

use app\models\KladrForm;
use app\models\Request;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use Kladr;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $res = '';
        $model = new KladrForm();
        if ($model->load(Yii::$app->request->post())) {
            $res = $model->findOneRes();
        }
        return $this->render('index', ['model' => $model, 'res' => $res]);
    }

    public function actionIndex1()
    {
        $api = new \Yandex\Geo\Api();

// Можно искать по точке
        $api->setPoint(30.5166187, 50.4452705);

// Или можно икать по адресу
        $api->setQuery('Тверская 6');

// Настройка фильтров
        $api
            ->setLimit(1) // кол-во результатов
            ->setLang(\Yandex\Geo\Api::LANG_US) // локаль ответа
            ->load();

        $response = $api->getResponse();
        $response->getFoundCount(); // кол-во найденных адресов
        $response->getQuery(); // исходный запрос
        $response->getLatitude(); // широта для исходного запроса
        $response->getLongitude(); // долгота для исходного запроса

// Список найденных точек
        $collection = $response->getList();
        foreach ($collection as $item) {
            $item->getAddress(); // вернет адрес
            $item->getLatitude(); // широта
            $item->getLongitude(); // долгота
            $item->getData(); // необработанные данные
        }
    }
    public function actionList($id)
    {
        $model = new KladrForm();
        $model->address = $id;
        $res = $model->findAllRes();
        return $this->render('list', ['model' => $model, 'results' => $res]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
